"""Test client decorator."""
from typing import Any

import pytest

from jsonrpc2pyclient.decorator import rpc_client
from jsonrpc2pyclient.wsclient import AsyncRPCWSClient

transport = AsyncRPCWSClient("ws://localhost:9000/api")


@rpc_client(transport=transport)
class TestClient:
    @rpc_client(transport=transport, method_prefix="math.")
    class MathClient:
        async def add(self, a: int, b: int, **headers: Any) -> int:
            ...

    math = MathClient()

    @staticmethod
    async def connect() -> None:
        """Connect to WebSocket server."""
        await transport.connect()

    @staticmethod
    async def close() -> None:
        """Close connection to WebSocket server."""
        await transport.close()


@pytest.mark.asyncio
async def test_decorator_client() -> None:
    client = TestClient()
    await client.connect()
    assert await client.math.add(1, 2, headers={}) == 3
    await client.close()
